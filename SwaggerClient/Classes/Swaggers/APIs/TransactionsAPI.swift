//
// TransactionsAPI.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation
import Alamofire


open class TransactionsAPI {
    /**

     - parameter hash: (path)  
     - parameter height: (query) optional block height, defaults to current tip (optional)
     - parameter completion: completion handler to receive the data and the error objects
     */
    open class func tx(hash: String, height: Int64? = nil, completion: @escaping ((_ data: TxResponse?,_ error: Error?) -> Void)) {
        txWithRequestBuilder(hash: hash, height: height).execute { (response, error) -> Void in
            completion(response?.body, error)
        }
    }


    /**
     - GET /thorchain/tx/{hash}

     - examples: [{contentType=application/json, example={
  "observed_tx" : {
    "signers" : [ "thor1f3s7q037eancht7sg0aj995dht25rwrnu4ats5", "thor1f3s7q037eancht7sg0aj995dht25rwrnu4ats5" ],
    "keysign_ms" : 10000,
    "tx" : {
      "chain" : "BTC",
      "coins" : [ {
        "amount" : "100000",
        "decimals" : 6,
        "asset" : "BTC.BTC"
      }, {
        "amount" : "100000",
        "decimals" : 6,
        "asset" : "BTC.BTC"
      } ],
      "gas" : [ null, null ],
      "memo" : "ADD:BTC.BTC:thor1zupk5lmc84r2dh738a9g3zscavannjy3nzplwt",
      "to_address" : "bcrt1qf3s7q037eancht7sg0aj995dht25rwrnqsf45e",
      "id" : "CF524818D42B63D25BBA0CCC4909F127CAA645C0F9CD07324F2824CC151A64C7",
      "from_address" : "bcrt1q0s4mg25tu6termrk8egltfyme4q7sg3h8kkydt"
    },
    "aggregator_target" : "0x0a44986b70527154e9F4290eC14e5f0D1C861822",
    "out_hashes" : [ "E17A0906E015F0C343691C18E475C8CB5F3F6C63F5BCDE0F3A341909763CC92B", "E17A0906E015F0C343691C18E475C8CB5F3F6C63F5BCDE0F3A341909763CC92B" ],
    "aggregator" : "0x69800327b38A4CeF30367Dec3f64c2f2386f3848",
    "observed_pub_key" : "thorpub1addwnpepq27ck6u44zl8qqdnmzjjc8rg72amrxrsp42p9vd7kt6marhy6ww76z8shwe",
    "finalise_height" : 7581334,
    "block_height" : 7581334,
    "aggregator_target_limit" : "0x0a44986b70527154e9F4290eC14e5f0D1C861822",
    "status" : "done"
  },
  "keysign_metric" : {
    "tx_id" : "tx_id",
    "node_tss_times" : [ {
      "address" : "address",
      "tss_time" : 0
    }, {
      "address" : "address",
      "tss_time" : 0
    } ]
  }
}}]
     - parameter hash: (path)  
     - parameter height: (query) optional block height, defaults to current tip (optional)

     - returns: RequestBuilder<TxResponse> 
     */
    open class func txWithRequestBuilder(hash: String, height: Int64? = nil) -> RequestBuilder<TxResponse> {
        var path = "/thorchain/tx/{hash}"
        let hashPreEscape = "\(hash)"
        let hashPostEscape = hashPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        path = path.replacingOccurrences(of: "{hash}", with: hashPostEscape, options: .literal, range: nil)
        let URLString = SwaggerClientAPI.basePath + path
        let parameters: [String:Any]? = nil
        var url = URLComponents(string: URLString)
        url?.queryItems = APIHelper.mapValuesToQueryItems([
                        "height": height?.encodeToJSON()
        ])


        let requestBuilder: RequestBuilder<TxResponse>.Type = SwaggerClientAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "GET", URLString: (url?.string ?? URLString), parameters: parameters, isBody: false)
    }
    /**

     - parameter hash: (path)  
     - parameter height: (query) optional block height, defaults to current tip (optional)
     - parameter completion: completion handler to receive the data and the error objects
     */
    open class func txSigners(hash: String, height: Int64? = nil, completion: @escaping ((_ data: TxSignersResponse?,_ error: Error?) -> Void)) {
        txSignersWithRequestBuilder(hash: hash, height: height).execute { (response, error) -> Void in
            completion(response?.body, error)
        }
    }


    /**
     - GET /thorchain/tx/{hash}/signers

     - examples: [{contentType=application/json, example={
  "tx" : {
    "signers" : [ "thor1f3s7q037eancht7sg0aj995dht25rwrnu4ats5", "thor1f3s7q037eancht7sg0aj995dht25rwrnu4ats5" ],
    "keysign_ms" : 10000,
    "tx" : {
      "chain" : "BTC",
      "coins" : [ {
        "amount" : "100000",
        "decimals" : 6,
        "asset" : "BTC.BTC"
      }, {
        "amount" : "100000",
        "decimals" : 6,
        "asset" : "BTC.BTC"
      } ],
      "gas" : [ null, null ],
      "memo" : "ADD:BTC.BTC:thor1zupk5lmc84r2dh738a9g3zscavannjy3nzplwt",
      "to_address" : "bcrt1qf3s7q037eancht7sg0aj995dht25rwrnqsf45e",
      "id" : "CF524818D42B63D25BBA0CCC4909F127CAA645C0F9CD07324F2824CC151A64C7",
      "from_address" : "bcrt1q0s4mg25tu6termrk8egltfyme4q7sg3h8kkydt"
    },
    "aggregator_target" : "0x0a44986b70527154e9F4290eC14e5f0D1C861822",
    "out_hashes" : [ "E17A0906E015F0C343691C18E475C8CB5F3F6C63F5BCDE0F3A341909763CC92B", "E17A0906E015F0C343691C18E475C8CB5F3F6C63F5BCDE0F3A341909763CC92B" ],
    "aggregator" : "0x69800327b38A4CeF30367Dec3f64c2f2386f3848",
    "observed_pub_key" : "thorpub1addwnpepq27ck6u44zl8qqdnmzjjc8rg72amrxrsp42p9vd7kt6marhy6ww76z8shwe",
    "finalise_height" : 7581334,
    "block_height" : 7581334,
    "aggregator_target_limit" : "0x0a44986b70527154e9F4290eC14e5f0D1C861822",
    "status" : "done"
  },
  "out_txs" : [ "out_txs", "out_txs" ],
  "outbound_height" : 1234,
  "updated_vault" : false,
  "tx_id" : "CF524818D42B63D25BBA0CCC4909F127CAA645C0F9CD07324F2824CC151A64C7",
  "actions" : [ null, null ],
  "txs" : [ null, null ],
  "finalised_height" : 7581334,
  "height" : 1234,
  "reverted" : false
}}]
     - parameter hash: (path)  
     - parameter height: (query) optional block height, defaults to current tip (optional)

     - returns: RequestBuilder<TxSignersResponse> 
     */
    open class func txSignersWithRequestBuilder(hash: String, height: Int64? = nil) -> RequestBuilder<TxSignersResponse> {
        var path = "/thorchain/tx/{hash}/signers"
        let hashPreEscape = "\(hash)"
        let hashPostEscape = hashPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        path = path.replacingOccurrences(of: "{hash}", with: hashPostEscape, options: .literal, range: nil)
        let URLString = SwaggerClientAPI.basePath + path
        let parameters: [String:Any]? = nil
        var url = URLComponents(string: URLString)
        url?.queryItems = APIHelper.mapValuesToQueryItems([
                        "height": height?.encodeToJSON()
        ])


        let requestBuilder: RequestBuilder<TxSignersResponse>.Type = SwaggerClientAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "GET", URLString: (url?.string ?? URLString), parameters: parameters, isBody: false)
    }
}
