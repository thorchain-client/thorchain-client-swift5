//
// KeysignResponse.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct KeysignResponse: Codable {

    public var keysign: KeysignInfo?
    public var signature: String?

    public init(keysign: KeysignInfo? = nil, signature: String? = nil) {
        self.keysign = keysign
        self.signature = signature
    }


}
