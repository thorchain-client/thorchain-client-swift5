//
// VaultAddress.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct VaultAddress: Codable {

    public var chain: String?
    public var address: String?

    public init(chain: String? = nil, address: String? = nil) {
        self.chain = chain
        self.address = address
    }


}
