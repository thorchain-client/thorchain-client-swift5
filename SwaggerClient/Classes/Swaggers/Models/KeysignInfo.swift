//
// KeysignInfo.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct KeysignInfo: Codable {

    /** the block(s) in which a tx out item is scheduled to be signed and moved from the scheduled outbound queue to the outbound queue */
    public var height: Int64?
    public var txArray: [TxOutItem]

    public init(height: Int64? = nil, txArray: [TxOutItem]) {
        self.height = height
        self.txArray = txArray
    }

    public enum CodingKeys: String, CodingKey { 
        case height
        case txArray = "tx_array"
    }

}
