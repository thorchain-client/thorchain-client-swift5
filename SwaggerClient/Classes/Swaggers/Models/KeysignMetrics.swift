//
// KeysignMetrics.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct KeysignMetrics: Codable {

    public var txId: String?
    public var nodeTssTimes: [TssMetric]?

    public init(txId: String? = nil, nodeTssTimes: [TssMetric]? = nil) {
        self.txId = txId
        self.nodeTssTimes = nodeTssTimes
    }

    public enum CodingKeys: String, CodingKey { 
        case txId = "tx_id"
        case nodeTssTimes = "node_tss_times"
    }

}
