//
// NodePreflightStatus.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct NodePreflightStatus: Codable {

    /** the next status of the node */
    public var status: String
    /** the reason for the transition to the next status */
    public var reason: String
    public var code: Int64

    public init(status: String, reason: String, code: Int64) {
        self.status = status
        self.reason = reason
        self.code = code
    }


}
